# Create-React-App + Cypress + cypress-cucumber-preprocessor

This repo combines a React app scaffolded with **CRA**
with **cypress-cucumber-preprocessor** being used for **Cypress component tests**.

The first step was: `npx create-react-app . --template typescript`
The second step was simply adhering to a wonderful example found
[here](https://github.com/badeball/cypress-cucumber-preprocessor/tree/master/examples/ct-react-ts), provided as part of the badeball/cypress-cucumber-preprocessor repo. Thank you, repo maintainers.

Use at your own risk.